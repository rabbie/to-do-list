import java.io.*;
import java.util.Scanner;

public class ToDoList {
    static Scanner scanner = new Scanner(System.in);
    static File file = new File("To-Do List.ods");

public static void makeToDoList(){
    String yn;
        do {
            Scanner scanner = new Scanner(System.in);
            System.out.println("Enter your date in 'yyyy-mm-dd' format:");
            String enterDate = scanner.nextLine();
            System.out.println("Explain your task: ");
            String explainTask = scanner.nextLine();

                try {
                    PrintWriter writer;
                    FileOutputStream out = new FileOutputStream(file, true);
                    writer = new PrintWriter(out);
                    writer.append(enterDate).append(",").append(explainTask).append("\n");
                    writer.flush();
                    writer.close();

                } catch (Exception ex) {
                        System.out.println(ex.getMessage());
                    }
                        System.out.println("Do you want to add more task on To-Do List?" +"\n" + "Press [Y] for Yes and [N] for No.");
                        yn = scanner.next();

        } while (yn.equalsIgnoreCase("y"));

}
public static void viewToDoList(){

    try {
        String ny;
            do {
                Scanner scanner = new Scanner(System.in);
                System.out.println("Let's view your To-Do List. Enter date in 'yyyy-mm-dd' format: ");
                String searchList = scanner.nextLine();
                FileReader fReader = new FileReader(file);
                BufferedReader bReader = new BufferedReader(fReader);
                String line = null;

                do {
                    line = bReader.readLine();
                    String matchToDoList = line.split(",")[0];

                    if (matchToDoList.equals(searchList)) {
                        System.out.println("Your task on " + line.split(",")[0] + " is " + line.split(",")[1]);
                        break;
                    }

                } while (line != null);
                fReader.close();
                bReader.close();
                System.out.println("Do you want to check more?"+ "\n" + "Press [Y] for Yes and [N] for No.");
                ny = scanner.next();
            }while (ny.equalsIgnoreCase("y"));



    } catch (FileNotFoundException ex){
        System.out.println(ex.getMessage());

        } catch (IOException e) {
            e.printStackTrace();
    }
}
public static void main(String[] args) {
        System.out.println("""
                Select your action:\s
                1. Make your To-Do List.
                2. View your To-Do List.
                3. Exit from this program.
                """);
        int writeNewToDo = scanner.nextInt();

            if(writeNewToDo == 1){
                makeToDoList();
            }
            if(writeNewToDo == 2){
                viewToDoList();
            }
    }
}
